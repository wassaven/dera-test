// SPDX-License-Identifier: UNLICENSED

pragma solidity 0.8.20;

import "@openzeppelin/contracts/access/Ownable.sol";
import "@openzeppelin/contracts/token/ERC20/ERC20.sol";
import "@openzeppelin/contracts/token/ERC20/utils/SafeERC20.sol";

import "./interfaces/IStrategy.sol";

contract Treasury is ERC20, Ownable {
    using SafeERC20 for IERC20;

    struct StrategyInfo {
        IStrategy strategy;
        uint weight;
    }

    IERC20 public immutable uToken;

    StrategyInfo[] public strategies;
    uint public totalDeposit;
    uint public totalWeight;

    event StrategyAdded(address indexed strategy, uint weight, uint index);
    event StrategyWeightUpdated(uint index, uint weight);

    constructor(address uTokenAddr) ERC20("Treasury Share Token", "TST") Ownable(msg.sender) {
        uToken = IERC20(uTokenAddr);
    }

    function addStrategy(address strategy, uint weight) external onlyOwner returns (uint strIndex) {
        require(strategy != address(0) && weight > 0, "Invalid strategy");

        strategies.push(StrategyInfo({strategy: IStrategy(strategy), weight: weight}));
        totalWeight += weight;

        strIndex = strategies.length - 1;
        emit StrategyAdded(strategy, weight, strIndex);
    }

    /**
     * @notice Owner function to update strategy weight
     * @dev Allow setting zero weight to disable strategy.
     * @param strIndex Index of strategy in strategies array
     * @param weight New weight of given strategy
     */
    function updateWeight(uint strIndex, uint weight) external onlyOwner {
        for (uint i = 0; i < strategies.length; i++) {
            strategies[i].strategy.withdrawAll();
        }

        totalWeight = totalWeight + weight - strategies[strIndex].weight;
        strategies[strIndex].weight = weight;
        emit StrategyWeightUpdated(strIndex, weight);

        _invest();
    }

    function _invest() internal {
        uint investAmount = uToken.balanceOf(address(this));
        for (uint i = 0; i < strategies.length; i++) {
            StrategyInfo storage strategyInfo = strategies[i];
            uint amount = (investAmount * strategyInfo.weight * 1e18) / totalWeight / 1e18;
            uToken.transfer(address(strategyInfo.strategy), amount);
            strategyInfo.strategy.invest(amount);
        }
    }

    function withdraw(uint shareAmount) external {
        uint uAmount = (totalDeposit + this.pendingRewards()) / totalSupply();
        totalDeposit -= uAmount;

        for (uint i = 0; i < strategies.length; i++) {
            StrategyInfo storage strategyInfo = strategies[i];
            uint amount = (uAmount * strategyInfo.weight * 1e18) / totalWeight / 1e18;
            strategyInfo.strategy.withdraw(amount);
        }

        _burn(msg.sender, shareAmount);
    }

    function deposit(uint amount) external {
        uToken.safeTransferFrom(msg.sender, address(this), amount);

        for (uint i = 0; i < strategies.length; i++) {
            StrategyInfo storage strategyInfo = strategies[i];
            uint investAmount = (amount * strategyInfo.weight * 1e18) / totalWeight / 1e18;
            strategyInfo.strategy.invest(investAmount);
        }

        uint sharePrice = (amount * totalDeposit) / (totalDeposit + this.pendingRewards());
        uint shareAmount = amount / sharePrice;

        _mint(msg.sender, shareAmount);
        totalDeposit += amount;
    }

    function pendingRewards() external view returns (uint rewards) {
        for (uint i = 0; i < strategies.length; i++) {
            rewards += strategies[i].strategy.pendingReward();
        }
    }
}
