// SPDX-License-Identifier: UNLICENSED

pragma solidity 0.8.20;

import "@openzeppelin/contracts/token/ERC20/ERC20.sol";
import "../interfaces/IStrategy.sol";
import "../interfaces/IAToken.sol";

contract AaveStrategy is IStrategy {
    address public immutable treasury;
    IERC20 public immutable uToken;
    IAToken public immutable aToken;
    IPool public immutable aPool;

    uint totalDeposit;

    modifier onlyTreasury() {
        require(msg.sender == treasury, "!Treasury");
        _;
    }

    constructor(address treasury_, address uToken_, address aToken_) {
        treasury = treasury_;
        uToken = IERC20(uToken_);
        aToken = IAToken(aToken_);
        aPool = aToken.POOL();
    }

    function invest(uint amount) external onlyTreasury returns (uint) {
        uint beforeATokenBal = aToken.balanceOf(address(this));

        uToken.approve(address(aPool), amount);
        aPool.supply(address(uToken), amount, address(this), 0);
        uint afterATokenBal = aToken.balanceOf(address(this));
        totalDeposit += amount;

        return afterATokenBal - beforeATokenBal;
    }

    function withdraw(uint amount) external onlyTreasury returns (uint withdrawn) {
        uint beforeUTokenBal = uToken.balanceOf(address(this));
        aPool.withdraw(address(uToken), amount, address(this));
        uint afterUTokenBal = uToken.balanceOf(address(this));
        withdrawn = afterUTokenBal - beforeUTokenBal;
        totalDeposit -= amount;

        uToken.transfer(msg.sender, withdrawn);
    }

    function withdrawAll() external onlyTreasury {
        aPool.withdraw(address(uToken), totalDeposit, address(this));
        uToken.transfer(msg.sender, uToken.balanceOf(address(this)));
    }

    function harvest() external onlyTreasury {}

    function pendingReward() external view returns (uint) {
        return aToken.balanceOf(address(this)) - totalDeposit;
    }
}
