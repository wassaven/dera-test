// SPDX-License-Identifier: UNLICENSED

pragma solidity 0.8.20;

import "@openzeppelin/contracts/token/ERC20/IERC20.sol";

import "./IPool.sol";

interface IAToken is IERC20 {
    function POOL() external view returns (IPool);

    function scaledBalanceOf(address user) external view returns (uint);
}
