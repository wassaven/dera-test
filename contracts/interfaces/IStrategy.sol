// SPDX-License-Identifier: UNLICENSED

interface IStrategy {
    function invest(uint amount) external returns (uint);

    function withdraw(uint amount) external returns (uint);

    function withdrawAll() external;

    function harvest() external;

    function pendingReward() external view returns (uint);
}
